import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(private router: Router,
              private authenticationService: AuthenticationService) {}
              userData: FormGroup;
  onClick(value: any) {
    console.log(value);
    // if((this.userData.value.emailRef === 'abc@gmai.com' )&& (this.userData.value.emailRef === 'abc@gmai.com' )) {
    //   this.authenticationService.login(this.userData.value);
    // }
    this.authenticationService.login();
   }
}
