import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import {Storage} from '@ionic/storage'
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
authState = new BehaviorSubject(false);

  constructor(private platform: Platform,
              private storage: Storage,
              private router: Router) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
   }

   ifLoggedIn() {
    this.storage.get('loginCred').then((res) => {
      if (res) {
        this.authState.next(true);
      }
    });
   }

   login() {
     let dummy_response = {
      emailRef: 'abc@gmail.com',
      passRef: '123'
     };
     this.storage.set('loginCred', dummy_response).then((res) => {
       this.router.navigate(['dashboard']);
       this.authState.next(true);
       console.log(res);
     });
   }

   logout() {
     this.storage.remove('loginCred').then(() => {
       this.router.navigate(['home']);
       this.authState.next(false);
     })
   }

   isAuthenticated() {
     return this.authState.value;
   }
}
